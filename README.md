AdobeColor
===========

A python module that provides access to colors in Adobe Color and Adobe Swatch Exchange files.

Usage
--------

	:::python
	import adobecolor

	# Basic Usage
	colors = adobecolor.aco("test.aco")
	print colors[0]                    # Index by position
	print colors["I Have Shot Green"]  # or by name

	# HSV Colors
	name = "HSV Color Example"
	print colors[name]         # HSV values
	print colors[name].to_hex  # Convert to RGB hex

	# Works the same for LAB color