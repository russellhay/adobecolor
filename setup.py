from setuptools import setup

setup(
    name="AdobeColor",
    version="0.1",
    author="Russell Hay",
    author_email="me@russellhay.com",
    url="https://bitbucket.org/russellhay/adobecolor",
    packages=["adobecolor"],
    requires=[
        "colour (>=0.0.4)",
    ],
)
