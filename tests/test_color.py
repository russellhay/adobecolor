import os.path as path
from nose.tools import raises

import adobecolor
import adobecolor._color as color

def _testdata(*args):
    return path.abspath(path.join(
        path.dirname(__file__),
        "..",
        "test_data",
        *args
    ))

def test_colorspace_name():
    test_data = (
        (color._RGBColor, "RGB"),
        (color._HSBColor, "HSB"),
        (color._CMYKColor, "CMYK"),
        (color._LabColor, "Lab"),
        (color._WideCMYKColor, "WideCMYK"),
    )
    for type_, expected in test_data:
        yield check_colorspace_name, type_, expected

def check_colorspace_name(type_, expected):
    x = type_(0,0,0,0)
    assert x.colorspace == expected

def test_color_value():
    test_data = (
        (color._RGBColor, (0,0,0,0), "r=00 g=00 b=00", "000000"),
        (color._HSBColor, (32767, 62913, 65535/2, 0), "h=179 s=95 b=49", "000000"),
        # (color._CMYKColor, "CMYK"),
        # (color._LabColor, "Lab"),
        # (color._WideCMYKColor, "WideCMYK"),
    )

    for type_, args, expected, expected_hex in test_data:
        yield check_color_value, type_, args, expected, expected_hex

def check_color_value(type_, args, expected, expected_hex):
    x = type_(*args)
    print x.value
    assert x.value == expected
    print x.hex
    assert x.hex == expected_hex
