import os.path as path
from nose.tools import raises

import adobecolor

def _testdata(*args):
    return path.abspath(path.join(
        path.dirname(__file__),
        "..",
        "test_data",
        *args
    ))

ACO_FILE = _testdata("test1.aco")
ASE_FILE = _testdata("test1.ase")

def test_bvt():
    aco = adobecolor.aco(ACO_FILE)
    assert aco is not None

    assert aco.length == 16
    assert len(aco.keys()) == 16

    assert aco[0] is not None
    color = aco["Goblin Guts"]
    assert color is not None
    assert aco[0] == color
    assert aco[1] != color
    assert aco[1] == aco["Goblin Lipstick"]

    assert hasattr(color, "hex")
    assert color.hex is not None
    assert color.hex == "343332"

    assert iter(aco) is not None

@raises(adobecolor.InvalidFileTypeError)
def test_invalid_aco():
    aco = adobecolor.aco(ASE_FILE)
